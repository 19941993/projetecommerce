import { Component, OnInit } from '@angular/core';
import { AuthentificationService } from '../authentification.service';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.css']
})
export class MenuComponent implements OnInit {
  private user: Observable<string>;
  //pour la recherche des produits par marque on fait
 /* titre= 'recherche sur les produits';
  marque :string ='anti-oxydant'; */

  constructor(private authService: AuthentificationService, private router: Router) { 
    this.user = this.authService.getUser();
  }
  // pour la recherche la methode :
  /*setMarque ( value: string){
    this.marque=value;
  } */

  ngOnInit() {
    this.router.navigate(['/categories']);
  }

  deconnexion(){
    this.authService.disconnect();
    this.router.navigate(['/categories']);
  }

}
